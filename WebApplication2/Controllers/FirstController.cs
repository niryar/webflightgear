﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;
using System.Threading;
using System.Xml;
using WebApplication2.Models;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace WebApplication2.Controllers
{
    public class FirstController : Controller
    {
        XmlHandler xmlHandler;

        public FirstController()
        {
            this.xmlHandler = new XmlHandler();
        }

        //public static int counter = 0;
        public object Views { get; private set; }

        // GET: First
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetCoor()
        {
            PlaneInfo planeInfo = InfoModel.Instance.LoadData();
            Coordinates coor = planeInfo.Coor;
            return Json(coor, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPlaneInfo()
        {
            PlaneInfo planeInfo = InfoModel.Instance.LoadData();
            return Json(planeInfo, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DisplayPointOrData(string ip, int port)
        {
            IPAddress IP;
            if (IPAddress.TryParse(ip, out IP))
            {
                return RedirectToAction("DisplayPoint", new { ip, port });
            }
            else
            {
                string fileName = ip;
                int time = port;
                InfoModel.Instance.fileName = fileName;
                return RedirectToAction("LoadDataFromFile", new { fileName, time });
            }
        }

        [HttpGet]
        public ViewResult DisplayPoint(string ip, int port)
        {
            InfoModel.Instance.ConnectClient(ip, port);
            PlaneInfo planeInfo = InfoModel.Instance.LoadData();
            Coordinates coor = InfoModel.Instance.LoadData().Coor;
            Session["Lon"] = coor.Lon;
            Session["Lat"] = coor.Lat;
            return View();
        }

        [HttpGet]
        public ViewResult LoadDataFromFile(string fileName, int time)
        {
            Session["time"] = time;
            Session["listOfCoor"] = fileName;
            return View();
        }

        [HttpGet]
        public ActionResult GetCoorFromFile()
        {
            XmlHandler xmlHandler = new XmlHandler();
            List<Coordinates> coorArray = xmlHandler.ReadData(InfoModel.Instance.fileName);
            return Json(coorArray, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult DisplayPlaneMovement(string ip, int port, int time)
        {
            InfoModel.Instance.ConnectClient(ip, port);
            Coordinates coor = InfoModel.Instance.LoadData().Coor;
            Session["Lon"] = coor.Lon;
            Session["Lat"] = coor.Lat;
            Session["time"] = time;

            return View();
        }

        [WebMethod]
        public object SaveData(string fileName, string planeInfo)
        {
            this.xmlHandler.SavePlaneInfoToXml(fileName, planeInfo);

            return Json(HttpStatusCode.OK);
        }

        public ActionResult SavePlaneInfo(string ip, int port, int time, int duration, string file)
        {
            XmlHandler xmlHandler = new XmlHandler();
            InfoModel.Instance.ConnectClient(ip, port);
            Coordinates coor = InfoModel.Instance.LoadData().Coor;
            Session["Lon"] = coor.Lon;
            Session["Lat"] = coor.Lat;
            Session["time"] = time;
            Session["duration"] = duration;
            Session["file"] = file;

            return View();
        }
    }
}
