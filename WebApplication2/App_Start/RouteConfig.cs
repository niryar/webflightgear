﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            
            routes.MapRoute("DisplayPointOrData", "display/{ip}/{port}",
            defaults: new { controller = "First", action = "DisplayPointOrData" });

            routes.MapRoute("DisplayPlaneMovement", "display/{ip}/{port}/{time}",
            defaults: new { controller = "First", action = "DisplayPlaneMovement" });

            routes.MapRoute("SavePlaneInfo", "save/{ip}/{port}/{time}/{duration}/{file}",
            defaults: new { controller = "First", action = "SavePlaneInfo" });

            routes.MapRoute("LoadDataFromFile", "display/{file}/{time}",
           defaults: new { controller = "First", action = "LoadDataFromFile" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "First", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
