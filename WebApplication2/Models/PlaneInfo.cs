﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebApplication2.Models
{
    public class PlaneInfo
    {
        public PlaneInfo(Coordinates coor, double throttle, double rudder)
        {
            this.Coor = coor;
            this.Throttle = throttle;
            this.Rudder = rudder;
        }

        public PlaneInfo(double lon, double lat, double throttle, double rudder)
        {
            this.Coor = new Coordinates(lon, lat);
            this.Throttle = throttle;
            this.Rudder = rudder;
        }


        public Coordinates Coor { get; set; }
        public double Throttle { get; set; }
        public double Rudder { get; set; }


        public void ToXml(XmlWriter writer)
        {
            writer.WriteStartElement("PlaneInfo");
            writer.WriteElementString("Lon", this.Coor.Lon.ToString());
            writer.WriteElementString("Lat", this.Coor.Lat.ToString());
            writer.WriteElementString("Throttle", this.Throttle.ToString());
            writer.WriteElementString("Rudder", this.Rudder.ToString());
            writer.WriteEndElement();
        }
    }
}