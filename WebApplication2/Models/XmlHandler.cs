﻿using System;
using System.Xml;
using System.Text;
using System.Threading;
using System.IO;
using System.Web;
using System.Collections.Generic;

namespace WebApplication2.Models
{
    public class XmlHandler
    {
        public const string SCENARIO_FILE = "~/App_Data/{0}.xml";


        private Tuple<PlaneInfo, int> ExtractPlaneInfo(string info, int startFrom)
        {
            int i1, i2;
            i1 = info.IndexOf(':', startFrom);
            i2 = info.IndexOf(',', i1);
            double Lon = Convert.ToDouble(info.Substring(i1 + 1, i2 - i1 - 1));

            i1 = info.IndexOf(':', i2);
            i2 = info.IndexOf(',', i1);
            double Lat = Convert.ToDouble(info.Substring(i1 + 1, i2 - i1 - 1));

            i1 = info.IndexOf(':', i2);
            i2 = info.IndexOf(',', i1);
            double Throttle = Convert.ToDouble(info.Substring(i1 + 1, i2 - i1 - 1));

            i1 = info.IndexOf(':', i2);
            i2 = info.IndexOf('}', i1);
            double Rudder = Convert.ToDouble(info.Substring(i1 + 1, i2 - i1 - 1));

            return Tuple.Create(new PlaneInfo(Lon, Lat, Throttle, Rudder), i2);
        }

        public void SavePlaneInfoToXml(string fileName, string planeInfoStr)
        {
            int i = 0;
            PlaneInfo planeInfo;
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            string path = HttpContext.Current.Server.MapPath(String.Format(SCENARIO_FILE, fileName));
            Tuple<PlaneInfo, int> tuple;


            using (StreamWriter streamWriter = new StreamWriter(path, false))
            {

                XmlWriter writer = XmlWriter.Create(streamWriter);
                writer.WriteStartDocument();
                writer.WriteStartElement("PlanesInfos");

                while (i + 10 < planeInfoStr.Length)
                {
                    tuple = ExtractPlaneInfo(planeInfoStr, i);
                    i = tuple.Item2;
                    planeInfo = tuple.Item1;
                    planeInfo.ToXml(writer);
                    writer.Flush();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }
        }

        private double XmlParse(string data)
        {
            int i1, i2;
            i1 = data.IndexOf('>');
            i2 = data.IndexOf('<', i1);
            string str = data.Substring(i1 + 1, i2 - i1 - 1);
            return Convert.ToDouble(str);
        }

        public List<Coordinates> ReadData(string name)
        {
            string path = HttpContext.Current.Server.MapPath(String.Format(SCENARIO_FILE, name));
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            StreamReader streamReader = new StreamReader(path, true);
            XmlReader reader = XmlReader.Create(streamReader, settings);
            reader.MoveToContent();
            List<Coordinates> coordinatesList = new List<Coordinates>();
            string[] values = new string[4];
            int i = 0;

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Text:
                        values[i++] = reader.Value;
                        if (i >= 4)
                        {
                            i = 0;
                            coordinatesList.Add(new Coordinates(Convert.ToDouble(values[0]), Convert.ToDouble(values[1])));
                        }
                        break;

                    default:
                        break;
                }
            }
            return coordinatesList;
        }
    }
}