﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using WebApplication2.Models;

class InfoModel
{
    private static InfoModel _Instance = null;
    public TcpClient _client;
    bool _isConnected;
    private string ip;
    private int port;


    public static InfoModel Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = new InfoModel();
            }
            return _Instance;
        }
    }

    private InfoModel()
    {
        _isConnected = false;
    }

    public string fileName { get; set; }

    public void ConnectClient(string ip, int port)
    {
        if (_isConnected && this.ip == ip && this.port == port)
        {
            return;
        }
        else if (_isConnected)
        {
            _client.Close();
            _isConnected = false;
        }
        IPEndPoint iPEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
        _client = new TcpClient();
        _client.Connect(iPEndPoint);
        Console.WriteLine("You are connected");
        _isConnected = true;
        this.ip = ip;
        this.port = port;
    }

    public PlaneInfo LoadData()
    {
        StringBuilder stringBuilder = new StringBuilder();
        byte[][] Commands = new byte[4][];
        double[] nums = new double[4];
        Commands[0] = Encoding.ASCII.GetBytes("get /position/longitude-deg\r\n");
        Commands[1] = Encoding.ASCII.GetBytes("get /position/latitude-deg\r\n");
        Commands[2] = Encoding.ASCII.GetBytes("get /controls/engines/current-engine/throttle\r\n");
        Commands[3] = Encoding.ASCII.GetBytes("get /controls/flight/rudder\r\n");

        byte[] answerBuffer = new byte[248];
        int numberOfBytesRead;

        if (!_isConnected) return null;
        NetworkStream ns = _client.GetStream();
        for (int i = 0; i < 4; i++)
        {
            if (ns.CanWrite)
            {
                ns.Write(Commands[i], 0, Commands[i].Length);
            }
            if (ns.CanRead)
            {
                numberOfBytesRead = ns.Read(answerBuffer, 0, answerBuffer.Length);
                stringBuilder.AppendFormat("{0}", Encoding.ASCII.GetString(answerBuffer, 0, numberOfBytesRead));
                nums[i] = ParseData(stringBuilder.ToString());
            }
            stringBuilder.Clear();
            Array.Clear(answerBuffer, 0, answerBuffer.Length);
        }
        // Coordinates, Throttle, Rudder
        return new PlaneInfo(new Coordinates(nums[0], nums[1]), nums[2], nums[3]);
    }

    private double ParseData(string data)
    {
        int i1, i2;
        i1 = data.IndexOf('\'');
        i2 = data.IndexOf('\'', i1 + 1);
        string str = data.Substring(i1 + 1, i2 - i1 - 1);
        return Convert.ToDouble(str);
    }
}



