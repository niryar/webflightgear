﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebApplication2.Models
{
    public class Coordinates
    {
        public Coordinates(double Lon, double Lat) {
            this.Lon = Lon;
            this.Lat = Lat;
        }

        public Coordinates()
        {
            this.Lon = 0;
            this.Lat = 0;
        }

        public double Lon { get; set; }
        public double Lat { get; set; }     
    }
}